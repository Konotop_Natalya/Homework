"use strict";
class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    set name(val) {
        this._name = val;
    }
    get name() {
        return this._name;
    }
    set age(val) {
        this._age = val;
    }
    get age() {
        return this._age;
    }
    set salary(val) {
        this._salary = val;
    }
    get salary() {
        return this._salary;
    }
}
class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }
    get salary() {
        return this._salary * 3;
    }
    set lang(val) {
        this._lang = val;
    }
    get lang() {
        return this._lang;
    }
}
const programmer1 = new Programmer("Natasha", 30, 500, ["eng", "ua", "rum"]);
const programmer2 = new Programmer("Anna", 31, 600, ["pl", "ua"]);
const programmer3 = new Programmer("Alex", 25, 700, ["eng", "fr"]);
console.log(programmer1);
console.log(programmer2);
console.log(programmer3);

