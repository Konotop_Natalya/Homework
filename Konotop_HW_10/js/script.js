"use strict";
document.querySelector('ul').addEventListener('click', e => {
    if (e.target.tagName === "LI") {
        e.preventDefault();
        document.querySelectorAll("li.active, tabs-content li.active")
            .forEach(element => {
            element.classList.remove('active');
        })
        e.target.classList.add('active');

        const id = e.target.getAttribute ('data-id');
        document.querySelector(id).classList.add('active');
    }
})
