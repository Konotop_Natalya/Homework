"use strict";
const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];
const root = document.querySelector("#root");
const list = document.createElement("ul");
root.append(list);

function booksList() {
    books.forEach((item) => {
        try {
            if (!item.author) {
                throw new SyntaxError("Помилка! Не вказан author");
            }
            if (!item.name) {
                throw new SyntaxError("Помилка! Не вказанно name");
            }
            if (!item.price) {
                throw new SyntaxError("Помилка! Не вказан price");
            }
            let itemList = document.createElement("li");
            itemList.innerText = item.author + " " + item.name + " " + item.price;
            list.append(itemList);
        } catch (element) {
            console.log(element);
        }
    });
}
booksList();