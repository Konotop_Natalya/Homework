"use strict";
function createNewUser() {
    const firstName = prompt('Enter you name');
    const lastName = prompt('Enter you second name');
    const birthday = prompt('Enter you birthday dd.mm.yyyy','dd.mm.yyyy');
    const user = {
        firstName,
        lastName,
        getAge () {
            const arr = birthday.split('.')
            return ((new Date().getTime() - new Date(`${arr[2]}-${arr[1]}-${arr[0]}`)) / (24 * 3600 * 365 * 1000)) | 0;
        },
        getLogin () {
            return (firstName.charAt(0) + lastName).toLowerCase();
        },
        getPassword () {
            const arr = birthday.split('.')
            return (firstName.charAt(0).toUpperCase()+lastName.toLowerCase()+arr[2])
        },
    }
    Object.defineProperty(user, "firstName", {
        writable: false,
        configurable: true,
    })
    user.setUserName = function (firstName) {
        Object.defineProperty(this, "firstName", {
            value: firstName
        })
    }
    Object.defineProperty(user, "lastName", {
        writable: false,
        configurable: true,
    })
    user.setSecondName = function (lastName) {
        Object.defineProperty(this, "lastName", {
            value: lastName
        })
    }
   return user
}
let newUserObj = createNewUser ();
console.log(newUserObj.getLogin());
console.log(newUserObj.getAge());
console.log(newUserObj.getPassword());
